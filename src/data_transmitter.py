import requests

def send_data_to_server(data):
    requestTemplate = {
        "id": 0,
        "position": (1,1),
        "occupied": False,
        "price": 0,
        "meta": {
            "address": "Francisciho",
            "value": 0.99
        }
    }
    
    def map_data_to_req(datum):
        req = dict(requestTemplate)
        req['id'] = datum[0]
        req['occupied'] = bool(datum[1])
        return req

    request = list(map(map_data_to_req, data))
    # for req in request:
        # print("REQ: ", req)
        # print("DATA: ", data)
    # print("REQUEST: ", request)
    try:
        response = requests.post("http://localhost:8080/api", json=request)
        print("RES: ", response)
    except Exception as e:
        print("Could not connect to OPD server api. Check if the server is running.")
        