# docker build -t opd-client .
# docker images
# docker ps
# docker run -it --rm -p 8080:8080 opd-client
# docker system prune -a

#ARG buildno
#ARG password
#RUN echo "Build number: $buildno"
#RUN script-requiring-password.sh "$password"

# Version of node available from the Docker Hub
# FROM debian:testing-slim
FROM python:2.7

RUN apt-get update
RUN apt-get install python-virtualenv libpython-dev alpine-pico nano sudo -y

# Replace 1000 with your user / group id
RUN export uid=1000 gid=1000 && \
    mkdir -p /home/developer && \
    echo "developer:x:${uid}:${gid}:Developer,,,:/home/developer:/bin/bash" >> /etc/passwd && \
    echo "developer:x:${uid}:" >> /etc/group && \
    echo "developer ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/developer && \
    chmod 0440 /etc/sudoers.d/developer && \
    chown ${uid}:${gid} -R /home/developer
USER developer
ENV HOME /home/developer
ENV QT_X11_NO_MITSHM 1

# Create app directory
COPY --chown=developer:developer . /home/developer/app
WORKDIR /home/developer/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
# If you are building your code for production
RUN virtualenv venv; \
    . venv/bin/activate; \
    pip install -r requirements.txt; \
    pip list; \
    exit

CMD /bin/bash
# CMD [ "npm", "run", "server:cluster" ]
# CMD npm start
