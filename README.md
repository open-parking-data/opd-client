# opd-client

Open Parking Data Client

## Run in Docker

```sh
docker build -t opdc .
```

```sh
docker run -ti --rm \
    -e DISPLAY=$DISPLAY \
    -v /tmp/.X11-unix:/tmp/.X11-unix \
    --privileged \
    -v /dev/video0:/dev/video0 \
    opdc
```

Activate virtualenv:

```sh
. venv/bin/activate
```

Run script

```
cd src/
python parking_detection.py
```

## Run in docker-compose

```sh
docker-compose run --rm opd-client
```

Activate virtualenv:

```sh
. venv/bin/activate
```

Run script

```
cd src/
python ./parking_detection.py
```
